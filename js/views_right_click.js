(function ($, Drupal, window, document) {
  'use strict';

  Drupal.behaviors.views_right_click = {
    attach: function (context, settings) {
      // Enable html5 context menus polyfill.
      $.contextMenu('html5', {
        animation: {duration: 0, show: 'fadeIn', hide: 'fadeOut'}
      });
    }
  };

})(jQuery, Drupal, this, this.document);
